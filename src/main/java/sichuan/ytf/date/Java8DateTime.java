package sichuan.ytf.date;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;

public class Java8DateTime {

    public static void main(String[] args) {
        change();
    }

    /**
     * 各种转换
     */
    static void change() {
        long timestamp = System.currentTimeMillis();
        System.out.println(timestamp);
        // long 转 Instant
        Instant it =Instant.ofEpochMilli(timestamp);
        System.out.println(it.getEpochSecond());
        System.out.println(it.getNano());
        
        // Instant 转 ZonedDateTime
        ZonedDateTime zdt = it.atZone(ZoneId.of("America/New_York"));
        System.out.println(zdt);
        ZonedDateTime zdtsh = zdt.withZoneSameInstant(ZoneId.of("Asia/Shanghai"));// 转换时区，需变量接收
        System.out.println(zdtsh);

        // ZonedDateTime 转 LocalDateTime
        LocalDateTime ldt = zdtsh.toLocalDateTime(); // 直接干掉时区
        System.out.println(ldt);

        // LocalDateTime 转 ZonedDateTime
        ZonedDateTime zdtny = ldt.atZone(ZoneId.of("America/New_York"));

        // ZonedDateTime 转 Instant
        Instant itny = zdtny.toInstant();
        System.out.println(itny.getEpochSecond());
        System.out.println(itny.getNano());
        
        long timestamp2 = itny.toEpochMilli();
        System.out.println(timestamp2);
    }
}
