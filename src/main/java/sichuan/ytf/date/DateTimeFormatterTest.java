package sichuan.ytf.date;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

public class DateTimeFormatterTest {

    public static void main(String[] args) throws InterruptedException {
        Instant now = Instant.now();
        System.out.println(now.getEpochSecond()); // 秒
        System.out.println(now.toEpochMilli()); // 毫秒
        System.out.println(System.currentTimeMillis());
        System.out.println(1000_000);
        test2();
//        test();
    }

    private static void test2() {

        LocalDateTime departureAtBeijing = LocalDateTime.of(2019, 9, 15, 13, 0, 0);
        int hours = 13;
        int minutes = 20;
        LocalDateTime arrivalAtNewYork = calculateArrivalAtNY(departureAtBeijing, hours, minutes);
        System.out.println(departureAtBeijing + " -> " + arrivalAtNewYork);
        // test:
        if (!LocalDateTime.of(2019, 10, 15, 14, 20, 0)
                .equals(calculateArrivalAtNY(LocalDateTime.of(2019, 10, 15, 13, 0, 0), 13, 20))) {
            System.err.println("测试失败!");
        } else if (!LocalDateTime.of(2019, 11, 15, 13, 20, 0)
                .equals(calculateArrivalAtNY(LocalDateTime.of(2019, 11, 15, 13, 0, 0), 13, 20))) {
            System.err.println("测试失败!");
        }

    }

    static LocalDateTime calculateArrivalAtNY(LocalDateTime bj, int h, int m) {
        bj = bj.plusHours(h);
        bj = bj.plusMinutes(m);
        ZonedDateTime atZone = bj.atZone(ZoneId.of("Asia/Shanghai"));
        return atZone.withZoneSameInstant(ZoneId.of("America/New_York")).toLocalDateTime();
    }

    private static void test() {
        LocalDateTime now = LocalDateTime.now();
        System.out.println(now);
        now.minusHours(1);
        System.out.println(now);
        LocalDateTime target = LocalDateTime.now();
        System.out.println(now.isBefore(target));

        ZonedDateTime zdt = ZonedDateTime.now();
//        ZonedDateTime zdt = ZonedDateTime.now(ZoneId.of("America/New_York")); // 用指定时区获取当前时间
//        ZonedDateTime zdt = ZonedDateTime.now(ZoneId.of("Asia/Shanghai"));
        System.out.println(zdt);

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm ZZZZ");
        System.out.println(formatter.format(zdt));

        DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss:SSS");
        System.out.println(df.format(zdt));

        DateTimeFormatter zhFormatter = DateTimeFormatter.ofPattern("yyyy MMM dd EE HH:mm", Locale.CHINA);
        System.out.println(zhFormatter.format(zdt));

        DateTimeFormatter usFormatter = DateTimeFormatter.ofPattern("E, MMMM/dd/yyyy HH:mm", Locale.US);
        System.out.println(usFormatter.format(zdt));

        System.out.println("-------------------");
        LocalDateTime ldt = LocalDateTime.now();

        System.out.println(df.format(ldt));

        System.out.println(zhFormatter.format(ldt));

        System.out.println(usFormatter.format(ldt));
    }
}
